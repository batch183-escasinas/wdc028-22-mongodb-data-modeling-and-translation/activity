
// Users Collection (Users.json)
{
	"_id":"user001",
	"firstName": "John",
	"lastName" : "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789"
}
// Orders Collection (Orders.json)

{
	"_id": "order001",
	"userID":"user001"
	"transactionDate" : "2022-05-10T15:00:00.00Z" ,
	"status": "For Shipment",
	"total" :  5000,

}
{
	"_id": "order002",
	"userID":"user001"
	"transactionDate" : "2022-05-10T15:00:00.00Z" ,
	"status": "For Shipment",
	"total" :  5000,

}
// orderProduct Collection (orderProduct.json)

{

	"_id": "orderProduct001",
	"orderID": "order001",
	"productID": "product001",
	"quantity": 25,
	"price":100,
	"subtotal":2500,
}
{

	"_id": "orderProduct002",
	"orderID": "order001",
	"productID": "product002",
	"quantity": 100,
	"price":25,
	"subtotal":2500,
}
{

	"_id": "orderProduct003",
	"orderID": "order002",
	"productID": "product003",
	"quantity": 5,
	"price":1000,
	"subtotal":5000,
}

// Product Collection (Product.json)

{

	"_id": "product001",
	"name": "Shampoo",
	"description": "this product",
	"price":100,
	"stocks":100,
	"isActive":true,
	"sku":"pd02562"

}
{

	"_id": "product002",
	"name": "Soap",
	"description": "this product",
	"price":25,
	"stocks":200,
	"isActive":true,
	"sku":"pd0212122"
}
{

	"_id": "product003",
	"name": "Necklace",
	"description": "this product",
	"price":5000,
	"stocks":10,
	"isActive":true,
	"sku":"NC0212122"
}


